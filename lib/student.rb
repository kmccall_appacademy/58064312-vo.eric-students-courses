# * `Student#courses` should return a list of the `Course`s in which
#   the student is enrolled.
# * `Student#enroll` should take a `Course` object, add it to the
#   student's list of courses, and update the `Course`'s list of
#   enrolled students.


class Student
  attr_reader :courses, :first_name, :last_name

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(course)
    return if courses.include?(course)
    raise "You are already taking the course" if already_taking?(course)

    self.courses << course
    course.students << self
  end

  def course_load
    course_list = {}
    self.courses.each do |course|
      if course_list[course.department] == nil
        course_list[course.department] = course.credits
      else
        course_list[course.department] += course.credits
      end
    end
    return course_list
  end

  def already_taking?(new_course)
    self.courses.each do |course|
      return true if new_course == course
    end
    return false
  end

end
